package id.telkomuniv.tel_usiagacovid_19.utils;

import android.content.Context;

import id.telkomuniv.tel_usiagacovid_19.R;

public class CommonUtils {

    public static String customDateShort(Context context, String date){
        String customMonth= "";
        switch (Integer.parseInt(date.substring(5, 7))){
            case 1:
                customMonth=context.getString(R.string.january);
                break;
            case 2:
                customMonth=context.getString(R.string.february);
                break;
            case 3:
                customMonth=context.getString(R.string.march);
                break;
            case 4:
                customMonth=context.getString(R.string.april);
                break;
            case 5:
                customMonth=context.getString(R.string.may);
                break;
            case 6:
                customMonth=context.getString(R.string.june);
                break;
            case 7:
                customMonth=context.getString(R.string.july);
                break;
            case 8:
                customMonth=context.getString(R.string.august);
                break;
            case 9:
                customMonth=context.getString(R.string.september);
                break;
            case 10:
                customMonth=context.getString(R.string.october);
                break;
            case 11:
                customMonth=context.getString(R.string.november);
                break;
            case 12:
                customMonth=context.getString(R.string.december);
                break;
        }

        String customDate = date.substring(8, 10)
                +" "
                +customMonth;

        return customDate;
    }

}
