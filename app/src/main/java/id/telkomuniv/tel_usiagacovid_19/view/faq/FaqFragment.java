package id.telkomuniv.tel_usiagacovid_19.view.faq;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.telkomuniv.tel_usiagacovid_19.R;
import id.telkomuniv.tel_usiagacovid_19.model.Faq;
import id.telkomuniv.tel_usiagacovid_19.model.Policy;
import id.telkomuniv.tel_usiagacovid_19.rest.ApiConfig;
import id.telkomuniv.tel_usiagacovid_19.rest.ApiService;
import id.telkomuniv.tel_usiagacovid_19.utils.MultiSwipeRefreshLayout;
import id.telkomuniv.tel_usiagacovid_19.view.dashboard.DashboardActivity;
import id.telkomuniv.tel_usiagacovid_19.view.policy.PolicyAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class FaqFragment extends Fragment {

    private static String TAG = "FaqFragment";

    @BindView(R.id.msrl_faq)
    MultiSwipeRefreshLayout multiSwipeRefreshLayout;
    @BindView(R.id.rv_faq)
    RecyclerView recyclerView;

    private DashboardActivity activity;
    private ApiService apiService;
    private List<Faq> faqList;

    public FaqFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public FaqFragment(DashboardActivity dashboardActivity) {
        this.activity = dashboardActivity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_faq, container, false);

        ButterKnife.bind(this, view);
        apiService = ApiConfig.getApiServiceTelu(getContext());
        initMultiSwipeRefresh();
        getDataFaq();

        return view;
    }

    private void initMultiSwipeRefresh() {
        multiSwipeRefreshLayout.setSwipeableChildren(R.id.sv_faq);
        multiSwipeRefreshLayout.setRefreshing(true);
        multiSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDataFaq();
            }
        });
    }

    private void getDataFaq() {
        faqList = new ArrayList<>();
        Call<List<Faq>> call = apiService.indexFaq();
        call.enqueue(new Callback<List<Faq>>() {
            @Override
            public void onResponse(Call<List<Faq>> call, Response<List<Faq>> response) {
                if (response.isSuccessful()){
                    Log.e(TAG, "response success : " + response.raw());
                    Log.e(TAG, "response success : " + response.body());
                    faqList = response.body();
                    showData();
                }
                else{
                    Log.e(TAG, "response is not success : " + response.raw());
                    Log.e(TAG, "response is not success : " + response.message());
                    Log.e(TAG, "response is not success : " + response.toString());
                    Log.e(TAG, "response is not success : " + response.code());
                    Log.e(TAG, "response is not success : " + response.errorBody());
                    Log.e(TAG, "response is not success : " + response.headers());
                    showToast(getResources().getString(R.string.failed_to_get_data));
                    multiSwipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<List<Faq>> call, Throwable t) {
                Log.e(TAG, "onFailure : " + t.getMessage());
                showToast(getResources().getString(R.string.there_is_an_error));
                multiSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void showData() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        FaqAdapter adapter = new FaqAdapter(getContext(), faqList);
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);

        multiSwipeRefreshLayout.setRefreshing(false);
    }

    private void showToast(String s){
        Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
    }

}
