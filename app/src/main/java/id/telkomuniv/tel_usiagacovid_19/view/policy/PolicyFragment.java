package id.telkomuniv.tel_usiagacovid_19.view.policy;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.anychart.AnyChartView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.telkomuniv.tel_usiagacovid_19.R;
import id.telkomuniv.tel_usiagacovid_19.model.Global;
import id.telkomuniv.tel_usiagacovid_19.model.Policy;
import id.telkomuniv.tel_usiagacovid_19.model.Province;
import id.telkomuniv.tel_usiagacovid_19.model.Timeline;
import id.telkomuniv.tel_usiagacovid_19.rest.ApiConfig;
import id.telkomuniv.tel_usiagacovid_19.rest.ApiService;
import id.telkomuniv.tel_usiagacovid_19.utils.MultiSwipeRefreshLayout;
import id.telkomuniv.tel_usiagacovid_19.view.dashboard.DashboardActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class PolicyFragment extends Fragment {

    private static String TAG = "PolicyFragment";

    @BindView(R.id.msrl_policy)
    MultiSwipeRefreshLayout multiSwipeRefreshLayout;
    @BindView(R.id.rv_policy)
    RecyclerView recyclerView;

    private DashboardActivity activity;
    private ApiService apiService;
    private List<Policy> policyList;
    private List<String> policyCategoryList;

    public PolicyFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public PolicyFragment(DashboardActivity dashboardActivity) {
        this.activity = dashboardActivity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_policy, container, false);

        ButterKnife.bind(this, view);
        apiService = ApiConfig.getApiServiceTelu(getContext());
        initMultiSwipeRefresh();
        getDataPolicy();

        return view;
    }

    private void initMultiSwipeRefresh() {
        multiSwipeRefreshLayout.setSwipeableChildren(R.id.rv_policy);
        multiSwipeRefreshLayout.setRefreshing(true);
        multiSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDataPolicy();
            }
        });
    }

    private void getDataPolicy() {
        policyList = new ArrayList<>();
        Call<List<Policy>> call = apiService.indexPolicy();
        call.enqueue(new Callback<List<Policy>>() {
            @Override
            public void onResponse(Call<List<Policy>> call, Response<List<Policy>> response) {
                if (response.isSuccessful()){
                    Log.e(TAG, "response success : " + response.raw());
                    Log.e(TAG, "response success : " + response.body());
                    policyList = response.body();
                    groupingData();
                }
                else{
                    Log.e(TAG, "response is not success : " + response.raw());
                    Log.e(TAG, "response is not success : " + response.message());
                    Log.e(TAG, "response is not success : " + response.toString());
                    Log.e(TAG, "response is not success : " + response.code());
                    Log.e(TAG, "response is not success : " + response.errorBody());
                    Log.e(TAG, "response is not success : " + response.headers());
                    showToast(getResources().getString(R.string.failed_to_get_data));
                    multiSwipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<List<Policy>> call, Throwable t) {
                Log.e(TAG, "onFailure : " + t.getMessage());
                showToast(getResources().getString(R.string.there_is_an_error));
                multiSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void groupingData() {
        //set all category
        policyCategoryList = new ArrayList<>();
        for (int i=0; i<policyList.size(); i++){
            boolean isMatch = false;
            for (int j=0; j<policyCategoryList.size(); j++){
                if (policyList.get(i).getKategori().equals(policyCategoryList.get(j)))
                    isMatch=true;
            }
            if (!isMatch)
                policyCategoryList.add(policyList.get(i).getKategori());
        }

        //sort list by category
        List<Policy> policyListTemp = new ArrayList<>();
        for (int i=0; i<policyCategoryList.size(); i++){
            Policy policy = new Policy();
            policy.setKategori(policyCategoryList.get(i));
            policyListTemp.add(policy);
            for (int j=0; j<policyList.size(); j++){
                if (policyList.get(j).getKategori().equals(policyCategoryList.get(i)))
                    policyListTemp.add(policyList.get(j));
            }
        }
        policyList = policyListTemp;

        showData();
    }

    private void showData() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        PolicyAdapter adapter = new PolicyAdapter(getContext(), policyList);
        recyclerView.setAdapter(adapter);
        //recyclerView.setNestedScrollingEnabled(false);

        multiSwipeRefreshLayout.setRefreshing(false);
    }

    private void showToast(String s){
        Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
    }

}
