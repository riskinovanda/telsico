package id.telkomuniv.tel_usiagacovid_19.view;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.telkomuniv.tel_usiagacovid_19.R;
import id.telkomuniv.tel_usiagacovid_19.model.Policy;
import id.telkomuniv.tel_usiagacovid_19.rest.ApiService;
import id.telkomuniv.tel_usiagacovid_19.utils.MultiSwipeRefreshLayout;
import id.telkomuniv.tel_usiagacovid_19.view.dashboard.DashboardActivity;

public class OurTeamActivity extends AppCompatActivity {

    private static String TAG = "OurTeamActivity";

    @BindView(R.id.msrl_our_team)
    MultiSwipeRefreshLayout multiSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_our_team);

        ButterKnife.bind(this);
        initMultiSwipeRefresh();
        getData();
    }

    private void initMultiSwipeRefresh() {
        multiSwipeRefreshLayout.setSwipeableChildren(R.id.sv_our_team);
        multiSwipeRefreshLayout.setRefreshing(true);
        multiSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });
    }

    private void getData() {
        multiSwipeRefreshLayout.setRefreshing(false);
    }

    @OnClick(R.id.cv_back_our_team) void cvBackOnClick(){
        onBackPressed();
    }
}
