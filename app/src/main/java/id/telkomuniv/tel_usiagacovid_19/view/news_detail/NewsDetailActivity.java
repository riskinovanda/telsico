package id.telkomuniv.tel_usiagacovid_19.view.news_detail;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.telkomuniv.tel_usiagacovid_19.R;
import id.telkomuniv.tel_usiagacovid_19.model.News;
import id.telkomuniv.tel_usiagacovid_19.utils.Constant;

import static android.text.Layout.JUSTIFICATION_MODE_INTER_WORD;

public class NewsDetailActivity extends AppCompatActivity {

    @BindView(R.id.tv_title_news_detail)
    TextView tvTitle;
    @BindView(R.id.iv_image_news_detail)
    ImageView ivImage;
    @BindView(R.id.tv_content_news_detail)
    TextView tvContent;

    News news;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);

        ButterKnife.bind(this);
        news = (News) getIntent().getSerializableExtra(Constant.PUT_EXTRA_NEWS);

        showData();
    }

    private void showData() {
        tvTitle.setText(news.getJudul());
        tvContent.setText(news.getIsi());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            tvContent.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
        }
        Picasso.get().load(news.getGambar()).into(ivImage);
    }

    @OnClick(R.id.cv_back_news_detail) void cvBackOnclick(){
        onBackPressed();
    }
}
