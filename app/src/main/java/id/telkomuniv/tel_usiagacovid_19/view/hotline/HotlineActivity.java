package id.telkomuniv.tel_usiagacovid_19.view.hotline;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.telkomuniv.tel_usiagacovid_19.R;
import id.telkomuniv.tel_usiagacovid_19.model.Hotline;
import id.telkomuniv.tel_usiagacovid_19.model.Policy;
import id.telkomuniv.tel_usiagacovid_19.rest.ApiConfig;
import id.telkomuniv.tel_usiagacovid_19.rest.ApiService;
import id.telkomuniv.tel_usiagacovid_19.utils.MultiSwipeRefreshLayout;
import id.telkomuniv.tel_usiagacovid_19.view.policy.PolicyAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.CAMERA;

public class HotlineActivity extends AppCompatActivity {

    private static String TAG = "HotlineActivity";

    @BindView(R.id.msrl_hotline)
    MultiSwipeRefreshLayout multiSwipeRefreshLayout;
    @BindView(R.id.rv_hotline)
    RecyclerView recyclerView;

    private static final int REQUEST_PHONE_CALL = 1;
    private ApiService apiService;
    private List<Hotline> hotlineList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotline);

        ButterKnife.bind(this);
        apiService = ApiConfig.getApiServiceTelu(this);
        initMultiSwipeRefresh();
        getData();
    }

    private void initMultiSwipeRefresh() {
        multiSwipeRefreshLayout.setSwipeableChildren(R.id.rv_hotline);
        multiSwipeRefreshLayout.setRefreshing(true);
        multiSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });
    }

    private void getData() {
        hotlineList = new ArrayList<>();
        Call<List<Hotline>> call = apiService.indexHotline();
        call.enqueue(new Callback<List<Hotline>>() {
            @Override
            public void onResponse(Call<List<Hotline>> call, Response<List<Hotline>> response) {
                if (response.isSuccessful()){
                    Log.e(TAG, "response success : " + response.raw());
                    Log.e(TAG, "response success : " + response.body());
                    hotlineList = response.body();
                    showData();
                }
                else{
                    Log.e(TAG, "response is not success : " + response.raw());
                    Log.e(TAG, "response is not success : " + response.message());
                    Log.e(TAG, "response is not success : " + response.toString());
                    Log.e(TAG, "response is not success : " + response.code());
                    Log.e(TAG, "response is not success : " + response.errorBody());
                    Log.e(TAG, "response is not success : " + response.headers());
                    showToast(getResources().getString(R.string.failed_to_get_data));
                    multiSwipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<List<Hotline>> call, Throwable t) {
                Log.e(TAG, "onFailure : " + t.getMessage());
                showToast(getResources().getString(R.string.there_is_an_error));
                multiSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void showData() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        HotlineAdapter adapter = new HotlineAdapter(this, hotlineList, this);
        recyclerView.setAdapter(adapter);
        //recyclerView.setNestedScrollingEnabled(false);

        multiSwipeRefreshLayout.setRefreshing(false);
    }

    public void callPermissioPhoneCall(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(!checkPermission())
                requestPermission();
        }
    }

    public boolean checkPermission(){
        return (ContextCompat.checkSelfPermission(this, CALL_PHONE)
                == PackageManager.PERMISSION_GRANTED);
    }

    public void requestPermission(){
        ActivityCompat.requestPermissions(this, new String[]{CALL_PHONE}, REQUEST_PHONE_CALL);
    }

    @OnClick(R.id.cv_back_hotline) void cvBackOnClick(){
        onBackPressed();
    }

    private void showToast(String s){
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.cv_line_hotline) void cvLineOnClick(){
        String url = "https://line.me/R/home/public/main?id=telkomuniversity";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    @OnClick(R.id.cv_wa_hotline) void cvWaOnClick(){
        String url = "https://api.whatsapp.com/send?phone=628112124447";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }
}
