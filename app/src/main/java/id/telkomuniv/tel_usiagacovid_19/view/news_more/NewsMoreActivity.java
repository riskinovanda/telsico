package id.telkomuniv.tel_usiagacovid_19.view.news_more;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.telkomuniv.tel_usiagacovid_19.R;
import id.telkomuniv.tel_usiagacovid_19.model.News;
import id.telkomuniv.tel_usiagacovid_19.utils.Constant;
import id.telkomuniv.tel_usiagacovid_19.view.home.NewsHomeAdapter;

public class NewsMoreActivity extends AppCompatActivity {

    @BindView(R.id.rv_news_more)
    RecyclerView rvNews;

    List<News> newsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_more);

        ButterKnife.bind(this);
        newsList = (List<News>) getIntent().getSerializableExtra(Constant.PUT_EXTRA_NEWS_LIST);
        showData();
    }

    private void showData() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvNews.setHasFixedSize(true);
        rvNews.setLayoutManager(linearLayoutManager);
        NewsMoreAdapter adapter = new NewsMoreAdapter(this, newsList);
        rvNews.setAdapter(adapter);
    }

    @OnClick(R.id.cv_back_news_more) void cvBackOnclick(){
        onBackPressed();
    }
}
