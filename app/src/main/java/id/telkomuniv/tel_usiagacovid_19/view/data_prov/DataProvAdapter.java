package id.telkomuniv.tel_usiagacovid_19.view.data_prov;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import id.telkomuniv.tel_usiagacovid_19.R;
import id.telkomuniv.tel_usiagacovid_19.model.Country;
import id.telkomuniv.tel_usiagacovid_19.model.Province;
import id.telkomuniv.tel_usiagacovid_19.view.data_global.DataGlobalAdapter;

public class DataProvAdapter extends RecyclerView.Adapter<DataProvAdapter.ViewHolder> {

    Context context;
    List<Province> provinceList;

    public DataProvAdapter(Context context, List<Province> provinceList) {
        this.context = context;
        this.provinceList = provinceList;
    }

    @NonNull
    @Override
    public DataProvAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list_data_province, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataProvAdapter.ViewHolder viewHolder, int i) {
        viewHolder.tvNo.setText(String.valueOf(i+1));
        viewHolder.tvName.setText(provinceList.get(i).getProvinsi());
        viewHolder.tvPositif.setText(String.valueOf(provinceList.get(i).getKasus_Posi()));
        viewHolder.tvHealthy.setText(String.valueOf(provinceList.get(i).getKasus_Semb()));
        viewHolder.tvDead.setText(String.valueOf(provinceList.get(i).getKasus_Meni()));
    }

    @Override
    public int getItemCount() {
        return provinceList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvNo;
        TextView tvName;
        TextView tvPositif;
        TextView tvHealthy;
        TextView tvDead;

        public ViewHolder(View itemView) {
            super(itemView);
            tvNo = itemView.findViewById(R.id.tv_no_item_list_data_prov);
            tvName = itemView.findViewById(R.id.tv_name_item_list_data_prov);
            tvPositif = itemView.findViewById(R.id.tv_positif_item_list_data_prov);
            tvHealthy = itemView.findViewById(R.id.tv_healthy_item_list_data_prov);
            tvDead = itemView.findViewById(R.id.tv_death_item_list_data_prov);
        }
    }
}
