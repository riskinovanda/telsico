package id.telkomuniv.tel_usiagacovid_19.view.data_global;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import id.telkomuniv.tel_usiagacovid_19.R;
import id.telkomuniv.tel_usiagacovid_19.model.Country;
import id.telkomuniv.tel_usiagacovid_19.model.Policy;
import id.telkomuniv.tel_usiagacovid_19.view.policy.PolicyAdapter;

public class DataGlobalAdapter extends RecyclerView.Adapter<DataGlobalAdapter.ViewHolder> {

    Context context;
    List<Country> countryList;

    public DataGlobalAdapter(Context context, List<Country> countryList) {
        this.context = context;
        this.countryList = countryList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list_data_global, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.tvNo.setText(String.valueOf(i+1));
        viewHolder.tvName.setText(countryList.get(i).getCountry_Region());
        viewHolder.tvPositif.setText(String.valueOf(countryList.get(i).getConfirmed()));
        viewHolder.tvHealthy.setText(String.valueOf(countryList.get(i).getRecovered()));
        viewHolder.tvDead.setText(String.valueOf(countryList.get(i).getDeaths()));
    }

    @Override
    public int getItemCount() {
        return countryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvNo;
        TextView tvName;
        TextView tvPositif;
        TextView tvHealthy;
        TextView tvDead;

        public ViewHolder(View itemView) {
            super(itemView);
            tvNo = itemView.findViewById(R.id.tv_no_item_list_data_global);
            tvName = itemView.findViewById(R.id.tv_name_item_list_data_global);
            tvPositif = itemView.findViewById(R.id.tv_positif_item_list_data_global);
            tvHealthy = itemView.findViewById(R.id.tv_healthy_item_list_data_global);
            tvDead = itemView.findViewById(R.id.tv_death_item_list_data_global);
        }
    }
}
