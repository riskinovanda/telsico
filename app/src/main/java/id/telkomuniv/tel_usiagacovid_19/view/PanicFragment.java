package id.telkomuniv.tel_usiagacovid_19.view;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import id.telkomuniv.tel_usiagacovid_19.R;
import id.telkomuniv.tel_usiagacovid_19.view.dashboard.DashboardActivity;


/**
 * A simple {@link Fragment} subclass.
 */
public class PanicFragment extends Fragment {


    public PanicFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public PanicFragment(DashboardActivity dashboardActivity) {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_panic, container, false);
    }

}
