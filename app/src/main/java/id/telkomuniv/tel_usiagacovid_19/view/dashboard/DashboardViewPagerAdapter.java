package id.telkomuniv.tel_usiagacovid_19.view.dashboard;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class DashboardViewPagerAdapter extends FragmentPagerAdapter {

    private Fragment homeFragment;
    private Fragment policyFragment;
    private Fragment panicFragment;
    private Fragment faqFragment;

    public DashboardViewPagerAdapter(FragmentManager fm, Fragment homeFragment, Fragment policyFragment, Fragment panicFragment, Fragment faqFragment) {
        super(fm);
        this.homeFragment = homeFragment;
        this.policyFragment = policyFragment;
        this.panicFragment = panicFragment;
        this.faqFragment = faqFragment;
    }

    @Override
    public Fragment getItem(int position) {
        if (position==0)
            return homeFragment;
        else if (position==1)
            return policyFragment;
        else if (position==2)
            return panicFragment;
        else
            return faqFragment;
    }

    @Override
    public int getCount() {
        return 4;
    }
}
