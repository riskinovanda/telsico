package id.telkomuniv.tel_usiagacovid_19.view.news_more;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import id.telkomuniv.tel_usiagacovid_19.R;
import id.telkomuniv.tel_usiagacovid_19.model.News;
import id.telkomuniv.tel_usiagacovid_19.utils.Constant;
import id.telkomuniv.tel_usiagacovid_19.view.news_detail.NewsDetailActivity;

public class NewsMoreAdapter extends RecyclerView.Adapter<NewsMoreAdapter.ViewHolder>{

    Context context;
    List<News> newsList;

    public NewsMoreAdapter(Context context, List<News> newsList) {
        this.context = context;
        this.newsList = newsList;
    }

    @NonNull
    @Override
    public NewsMoreAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_news_vertical, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsMoreAdapter.ViewHolder viewHolder, final int i) {
        Picasso.get().load(newsList.get(i).getGambar()).into(viewHolder.ivImage);
        viewHolder.tvTitle.setText(newsList.get(i).getJudul());

        viewHolder.cvItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, NewsDetailActivity.class);
                intent.putExtra(Constant.PUT_EXTRA_NEWS, newsList.get(i));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CardView cvItem;
        ImageView ivImage;
        TextView tvTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            cvItem = itemView.findViewById(R.id.cv_item_news_vertical);
            ivImage = itemView.findViewById(R.id.iv_image_item_news_vertical);
            tvTitle = itemView.findViewById(R.id.tv_title_item_news_vertical);
        }
    }
}
