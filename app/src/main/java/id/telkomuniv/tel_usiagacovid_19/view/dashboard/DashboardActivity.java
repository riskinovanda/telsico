package id.telkomuniv.tel_usiagacovid_19.view.dashboard;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.telkomuniv.tel_usiagacovid_19.R;
import id.telkomuniv.tel_usiagacovid_19.model.HotlinePrimary;
import id.telkomuniv.tel_usiagacovid_19.rest.ApiConfig;
import id.telkomuniv.tel_usiagacovid_19.rest.ApiService;
import id.telkomuniv.tel_usiagacovid_19.rest.CustomViewPager;
import id.telkomuniv.tel_usiagacovid_19.view.faq.FaqFragment;
import id.telkomuniv.tel_usiagacovid_19.view.home.HomeFragment;
import id.telkomuniv.tel_usiagacovid_19.view.PanicFragment;
import id.telkomuniv.tel_usiagacovid_19.view.policy.PolicyFragment;
import io.intercom.android.sdk.Intercom;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.CALL_PHONE;

public class DashboardActivity extends AppCompatActivity {

    private String TAG = "DashboardActivity";

    @BindView(R.id.cvp_dashboard)
    CustomViewPager customViewPager;
    @BindView(R.id.bnv_dashboard)
    BottomNavigationView bottomNavigationView;

    private static final int REQUEST_PHONE_CALL = 1;
    private ApiService apiService;
    private List<HotlinePrimary> hotlinePrimaryList;

    private Fragment homeFragment, policyFragment, panicFragment, faqFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        apiService = ApiConfig.getApiServiceTelu(this);

        ButterKnife.bind(this);
        homeFragment = new HomeFragment(this);
        policyFragment = new PolicyFragment(this);
        panicFragment = new PanicFragment(this);
        faqFragment = new FaqFragment(this);

        customViewPager.setAdapter(new DashboardViewPagerAdapter(getSupportFragmentManager(), homeFragment, policyFragment, panicFragment, faqFragment));
        customViewPager.setOffscreenPageLimit(4);
        //viewPager.setNestedScrollingEnabled(false);
        //customViewPager.beginFakeDrag();
        customViewPager.setPagingEnabled(false);

        initNavigation();
    }

    private void initNavigation() {
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                boolean isAvailable = false;

                switch (menuItem.getItemId()){
                    case R.id.nav_home:
                        customViewPager.setCurrentItem(0, false);
                        //Intercom.client().setLauncherVisibility(Intercom.Visibility.VISIBLE);
                        isAvailable = true;
                        break;
                    case R.id.nav_news:
                        customViewPager.setCurrentItem(1, false);
                        //Intercom.client().setLauncherVisibility(Intercom.Visibility.GONE);
                        isAvailable = true;
                        break;
                    case R.id.nav_panic_button:
                        showDialogPanicButton();
                        isAvailable = false;
                        break;
                    case R.id.nav_faq:
                        showToast(getString(R.string.under_development));
                        isAvailable = false;
                        break;
                }

                return isAvailable;
            }
        });

        /*spaceNavigationViewBackground.initWithSaveInstanceState(savedInstanceState);
        spaceNavigationViewBackground.addSpaceItem(new SpaceItem(getResources().getString(R.string.home), R.drawable.home));
        spaceNavigationViewBackground.addSpaceItem(new SpaceItem(getResources().getString(R.string.home), R.drawable.home));
        spaceNavigationViewBackground.showIconOnly();

        spaceNavigationView.initWithSaveInstanceState(savedInstanceState);
        spaceNavigationView.addSpaceItem(new SpaceItem(getResources().getString(R.string.home), R.drawable.home));
        spaceNavigationView.addSpaceItem(new SpaceItem(getResources().getString(R.string.transaction), R.drawable.ticket));
        spaceNavigationView.addSpaceItem(new SpaceItem(getResources().getString(R.string.location), R.drawable.location));
        spaceNavigationView.addSpaceItem(new SpaceItem(getResources().getString(R.string.account), R.drawable.user));
        spaceNavigationView.showIconOnly();

        if (requestDashboard!=0) {
            spaceNavigationView.changeCurrentItem(requestDashboard);
            //loadFragment(transactionFragment);
            viewPager.setCurrentItem(requestDashboard, true);
        }

        spaceNavigationView.setSpaceOnClickListener(new SpaceOnClickListener() {
            @Override
            public void onCentreButtonClick() {
                startActivity(new Intent(DashboardActivity.this, OrderScanActivity.class));
            }

            @Override
            public void onItemClick(int itemIndex, String itemName) {
                switch(itemIndex){
                    case 0:
                        viewPager.setCurrentItem(0, false);
                        break;
                    case 1:
                        viewPager.setCurrentItem(1, false);
                        break;
                    case 2:
                        viewPager.setCurrentItem(2, false);
                        break;
                    case 3:
                        viewPager.setCurrentItem(3, false);
                        break;
                }
            }

            @Override
            public void onItemReselected(int itemIndex, String itemName) {
                //Toast.makeText(DashboardActivity.this, itemIndex + " " + itemName, Toast.LENGTH_SHORT).show();
            }
        });*/
    }

    private void showDialogPanicButton() {
        new AlertDialog.Builder(this)
                .setMessage("Ingin menelpon hotline darurat?")
                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton("Lanjut", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Continue with delete operation
                        if (checkPermission()){
                            if (hotlinePrimaryList==null)
                                getDataHotlinePrimary();
                            else{
                                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + hotlinePrimaryList.get(0).getNomor()));
                                startActivity(intent);
                            }
                        }
                        else
                            callPermissioPhoneCall();
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton("Kembali", null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void getDataHotlinePrimary() {
        hotlinePrimaryList = new ArrayList<>();
        Call<List<HotlinePrimary>> call = apiService.indexHotlinePrimary();
        call.enqueue(new Callback<List<HotlinePrimary>>() {
            @Override
            public void onResponse(Call<List<HotlinePrimary>> call, Response<List<HotlinePrimary>> response) {
                if (response.isSuccessful()){
                    Log.e(TAG, "response success : " + response.raw());
                    Log.e(TAG, "response success : " + response.body());
                    hotlinePrimaryList = response.body();
                    if (!hotlinePrimaryList.isEmpty()){
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + hotlinePrimaryList.get(0).getNomor()));
                        startActivity(intent);
                    }
                }
                else{
                    Log.e(TAG, "response is not success : " + response.raw());
                    Log.e(TAG, "response is not success : " + response.message());
                    Log.e(TAG, "response is not success : " + response.toString());
                    Log.e(TAG, "response is not success : " + response.code());
                    Log.e(TAG, "response is not success : " + response.errorBody());
                    Log.e(TAG, "response is not success : " + response.headers());
                    showToast(getResources().getString(R.string.failed_to_get_data));
                }
            }

            @Override
            public void onFailure(Call<List<HotlinePrimary>> call, Throwable t) {
                Log.e(TAG, "onFailure : " + t.getMessage());
                showToast(getResources().getString(R.string.there_is_an_error));
            }
        });
    }

    private void showToast(String s){
        Toast.makeText(DashboardActivity.this, s, Toast.LENGTH_SHORT).show();
    }

    public void callPermissioPhoneCall(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(!checkPermission())
                requestPermission();
        }
    }

    public boolean checkPermission(){
        return (ContextCompat.checkSelfPermission(this, CALL_PHONE)
                == PackageManager.PERMISSION_GRANTED);
    }

    public void requestPermission(){
        ActivityCompat.requestPermissions(this, new String[]{CALL_PHONE}, REQUEST_PHONE_CALL);
    }
}
