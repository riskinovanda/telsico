package id.telkomuniv.tel_usiagacovid_19.view.faq;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import id.telkomuniv.tel_usiagacovid_19.R;
import id.telkomuniv.tel_usiagacovid_19.model.Faq;
import id.telkomuniv.tel_usiagacovid_19.model.Policy;
import id.telkomuniv.tel_usiagacovid_19.view.policy.PolicyAdapter;

public class FaqAdapter extends RecyclerView.Adapter<FaqAdapter.ViewHolder> {

    Context context;
    List<Faq> faqList;

    public FaqAdapter(Context context, List<Faq> faqList) {
        this.context = context;
        this.faqList = faqList;
    }

    @NonNull
    @Override
    public FaqAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_faq, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FaqAdapter.ViewHolder viewHolder, int i) {
        viewHolder.tvTitle.setText(faqList.get(i).getPertanyaan());
        viewHolder.tvAnswer.setText(faqList.get(i).getJawaban());
    }

    @Override
    public int getItemCount() {
        return faqList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        TextView tvAnswer;

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title_item_faq);
            tvAnswer = itemView.findViewById(R.id.tv_answer_item_faq);
        }
    }
}
