package id.telkomuniv.tel_usiagacovid_19.view.hotline;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import id.telkomuniv.tel_usiagacovid_19.R;
import id.telkomuniv.tel_usiagacovid_19.model.Hotline;
import id.telkomuniv.tel_usiagacovid_19.model.Policy;
import id.telkomuniv.tel_usiagacovid_19.view.policy.PolicyAdapter;

public class HotlineAdapter extends RecyclerView.Adapter<HotlineAdapter.ViewHolder> {

    Context context;
    List<Hotline> hotlineList;
    HotlineActivity activity;

    public HotlineAdapter(Context context, List<Hotline> hotlineList, HotlineActivity activity) {
        this.context = context;
        this.hotlineList = hotlineList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_hotline, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.tvName.setText(hotlineList.get(i).getNama());
        viewHolder.tvNumber.setText(hotlineList.get(i).getNomor());
        viewHolder.cvItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (activity.checkPermission()){
                    new AlertDialog.Builder(context)
                            .setMessage("Ingin menelpon nomer ini?")
                            // Specifying a listener allows you to take an action before dismissing the dialog.
                            // The dialog is automatically dismissed when a dialog button is clicked.
                            .setPositiveButton("Lanjut", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Continue with delete operation
                                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + hotlineList.get(i).getNomor()));
                                    context.startActivity(intent);
                                }
                            })

                            // A null listener allows the button to dismiss the dialog and take no further action.
                            .setNegativeButton("Kembali", null)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
                else{
                    activity.callPermissioPhoneCall();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return hotlineList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        TextView tvNumber;
        CardView cvItem;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name_item_hotline);
            tvNumber = itemView.findViewById(R.id.tv_number_item_hotline);
            cvItem = itemView.findViewById(R.id.cv_item_hotline);
        }
    }
}
