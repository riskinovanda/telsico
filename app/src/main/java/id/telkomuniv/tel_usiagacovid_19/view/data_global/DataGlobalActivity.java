package id.telkomuniv.tel_usiagacovid_19.view.data_global;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.telkomuniv.tel_usiagacovid_19.R;
import id.telkomuniv.tel_usiagacovid_19.model.Country;
import id.telkomuniv.tel_usiagacovid_19.model.CountryResponse;
import id.telkomuniv.tel_usiagacovid_19.model.Policy;
import id.telkomuniv.tel_usiagacovid_19.rest.ApiConfig;
import id.telkomuniv.tel_usiagacovid_19.rest.ApiService;
import id.telkomuniv.tel_usiagacovid_19.utils.MultiSwipeRefreshLayout;
import id.telkomuniv.tel_usiagacovid_19.view.policy.PolicyAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataGlobalActivity extends AppCompatActivity {

    private static String TAG = "DataGlobalActivity";

    @BindView(R.id.msrl_data_global)
    MultiSwipeRefreshLayout multiSwipeRefreshLayout;
    @BindView(R.id.rv_data_global)
    RecyclerView recyclerView;

    private ApiService apiService;
    private List<Country> countryList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_global);

        ButterKnife.bind(this);
        apiService = ApiConfig.getApiService(this);
        initMultiSwipeRefresh();
        getData();
    }

    private void initMultiSwipeRefresh() {
        multiSwipeRefreshLayout.setSwipeableChildren(R.id.rv_data_global);
        multiSwipeRefreshLayout.setRefreshing(true);
        multiSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });
    }

    private void getData() {
        countryList = new ArrayList<>();
        Call<List<CountryResponse>> call = apiService.indexCountry();
        call.enqueue(new Callback<List<CountryResponse>>() {
            @Override
            public void onResponse(Call<List<CountryResponse>> call, Response<List<CountryResponse>> response) {
                if (response.isSuccessful()){
                    Log.e(TAG, "response success : " + response.raw());
                    Log.e(TAG, "response success : " + response.body());
                    for (int i=0; i<response.body().size(); i++){
                        countryList.add(response.body().get(i).getCountry());
                    }
                    showData();
                }
                else{
                    Log.e(TAG, "response is not success : " + response.raw());
                    Log.e(TAG, "response is not success : " + response.message());
                    Log.e(TAG, "response is not success : " + response.toString());
                    Log.e(TAG, "response is not success : " + response.code());
                    Log.e(TAG, "response is not success : " + response.errorBody());
                    Log.e(TAG, "response is not success : " + response.headers());
                    showToast(getResources().getString(R.string.failed_to_get_data));
                    multiSwipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<List<CountryResponse>> call, Throwable t) {
                Log.e(TAG, "onFailure : " + t.getMessage());
                showToast(getResources().getString(R.string.there_is_an_error));
                multiSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void showData() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        DataGlobalAdapter adapter = new DataGlobalAdapter(this, countryList);
        recyclerView.setAdapter(adapter);

        multiSwipeRefreshLayout.setRefreshing(false);
    }

    @OnClick(R.id.cv_back_data_global) void cvBackOnClick(){
        onBackPressed();
    }

    private void showToast(String s){
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }
}
