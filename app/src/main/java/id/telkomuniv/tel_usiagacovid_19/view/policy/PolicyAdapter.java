package id.telkomuniv.tel_usiagacovid_19.view.policy;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import id.telkomuniv.tel_usiagacovid_19.R;
import id.telkomuniv.tel_usiagacovid_19.model.Policy;

public class PolicyAdapter extends RecyclerView.Adapter<PolicyAdapter.ViewHolder> {

    Context context;
    List<Policy> policyList;

    public PolicyAdapter(Context context, List<Policy> policyList) {
        this.context = context;
        this.policyList = policyList;
    }

    @NonNull
    @Override
    public PolicyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_policy, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PolicyAdapter.ViewHolder viewHolder, final int i) {
        viewHolder.setIsRecyclable(false);

        if (policyList.get(i).getId()==null){
            viewHolder.cvItem.setClickable(false);
            viewHolder.cvItem.setElevation(0);
            viewHolder.tvDate.setVisibility(View.GONE);
            viewHolder.tvName.setVisibility(View.GONE);
            viewHolder.ivIconDownload.setVisibility(View.GONE);
            viewHolder.tvCategory.setText(policyList.get(i).getKategori());
        }
        else{
            viewHolder.tvCategory.setVisibility(View.GONE);
            viewHolder.tvName.setText(policyList.get(i).getJudul());
            String date = policyList.get(i).getTanggal();
            String tmp = date.substring(0, date.indexOf(" "));
            date = date.substring(date.indexOf(" ")+1);
            tmp = tmp + "\n" + date.substring(0, date.indexOf(" "));
            date = date.substring(date.indexOf(" ")+1);
            tmp = tmp + "\n" + date;
            date = tmp;
            viewHolder.tvDate.setText(date);

            viewHolder.cvItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AlertDialog.Builder(context)
                            .setMessage("Ingin mengunduh dokumen?")
                            // Specifying a listener allows you to take an action before dismissing the dialog.
                            // The dialog is automatically dismissed when a dialog button is clicked.
                            .setPositiveButton("Download", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Continue with delete operation
                                    Intent intent = new Intent(Intent.ACTION_VIEW);
                                    intent.setData(Uri.parse(policyList.get(i).getLink()));
                                    context.startActivity(intent);
                                }
                            })

                            // A null listener allows the button to dismiss the dialog and take no further action.
                            .setNegativeButton("Kembali", null)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return policyList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvCategory;
        TextView tvName;
        TextView tvDate;
        ImageView ivIconDownload;
        CardView cvItem;

        public ViewHolder(View itemView) {
            super(itemView);
            tvCategory = itemView.findViewById(R.id.tv_category_item_policy);
            tvName = itemView.findViewById(R.id.tv_name_item_policy);
            tvDate = itemView.findViewById(R.id.tv_date_item_policy);
            ivIconDownload = itemView.findViewById(R.id.iv_icon_download_item_policy);
            cvItem = itemView.findViewById(R.id.cv_item_policy);
        }
    }
}
