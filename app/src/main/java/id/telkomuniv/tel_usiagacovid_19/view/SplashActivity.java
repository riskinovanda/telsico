package id.telkomuniv.tel_usiagacovid_19.view;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import id.telkomuniv.tel_usiagacovid_19.R;
import id.telkomuniv.tel_usiagacovid_19.model.AppVersion;
import id.telkomuniv.tel_usiagacovid_19.model.Global;
import id.telkomuniv.tel_usiagacovid_19.rest.ApiConfig;
import id.telkomuniv.tel_usiagacovid_19.rest.ApiService;
import id.telkomuniv.tel_usiagacovid_19.utils.Constant;
import id.telkomuniv.tel_usiagacovid_19.view.dashboard.DashboardActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    private static String TAG = "SplashActivity";

    private ApiService apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        apiService = ApiConfig.getApiServiceTelu(this);

        checkAppVersion();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        checkAppVersion();
    }

    private void checkAppVersion() {
        Call<List<AppVersion>> call = apiService.indexAppVersion();
        call.enqueue(new Callback<List<AppVersion>>() {
            @Override
            public void onResponse(Call<List<AppVersion>> call, Response<List<AppVersion>> response) {
                if (response.isSuccessful()){
                    Log.e(TAG, "response success : " + response.raw());
                    Log.e(TAG, "response success : " + response.body());
                    AppVersion appVersion = response.body().get(0);
                    if (appVersion.getLatest_version().equals(Constant.APP_VERSION)){
                        showToast("Versi aplikasi : "+Constant.APP_VERSION);
                        intentToDashboard();
                    }
                    else{
                        dialogAppVersion();
                    }
                }
                else{
                    Log.e(TAG, "response is not success : " + response.raw());
                    Log.e(TAG, "response is not success : " + response.message());
                    Log.e(TAG, "response is not success : " + response.toString());
                    Log.e(TAG, "response is not success : " + response.code());
                    Log.e(TAG, "response is not success : " + response.errorBody());
                    Log.e(TAG, "response is not success : " + response.headers());
                    showToast(getResources().getString(R.string.failed_to_get_data));
                    dialogAppVersion();
                }
            }

            @Override
            public void onFailure(Call<List<AppVersion>> call, Throwable t) {
                Log.e(TAG, "onFailure : " + t.getMessage());
                showToast(getResources().getString(R.string.there_is_an_error));
                dialogAppVersion();
            }
        });
    }

    private void dialogAppVersion(){
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        dialog.dismiss();
                        onBackPressed();
                        break;
                    case DialogInterface.BUTTON_POSITIVE:
                        //No button clicked
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("https://telkomuniversity.ac.id/siagacovid-19/"));
                        startActivity(intent);
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.this_app_version_is_deprecated))
                .setNegativeButton(R.string.cancel, dialogClickListener)
                .setPositiveButton(getString(R.string.download_app), dialogClickListener)
                .show();
    }

    private void intentToDashboard() {
        Thread splashThread = new Thread(){
            @Override
            public void run(){
                try {
                    sleep(2500);
                    startActivity(new Intent(getApplicationContext(), DashboardActivity.class));
                    finish();
                }catch(InterruptedException e){
                    //e.printStackTrace();
                    System.exit(1);
                }
            }
        };
        splashThread.start();
    }

    private void showToast(String s){
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }
}
