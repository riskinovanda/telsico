package id.telkomuniv.tel_usiagacovid_19.view.home;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Cartesian;
import com.anychart.core.cartesian.series.Line;
import com.anychart.data.Mapping;
import com.anychart.data.Set;
import com.anychart.enums.Anchor;
import com.anychart.enums.MarkerType;
import com.anychart.enums.TooltipPositionMode;
import com.anychart.graphics.vector.Stroke;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.telkomuniv.tel_usiagacovid_19.R;
import id.telkomuniv.tel_usiagacovid_19.model.DataResponse;
import id.telkomuniv.tel_usiagacovid_19.model.Global;
import id.telkomuniv.tel_usiagacovid_19.model.News;
import id.telkomuniv.tel_usiagacovid_19.model.Province;
import id.telkomuniv.tel_usiagacovid_19.model.ProvinceResponse;
import id.telkomuniv.tel_usiagacovid_19.model.Timeline;
import id.telkomuniv.tel_usiagacovid_19.model.TimelineResponse;
import id.telkomuniv.tel_usiagacovid_19.rest.ApiConfig;
import id.telkomuniv.tel_usiagacovid_19.rest.ApiService;
import id.telkomuniv.tel_usiagacovid_19.utils.CommonUtils;
import id.telkomuniv.tel_usiagacovid_19.utils.Constant;
import id.telkomuniv.tel_usiagacovid_19.utils.MultiSwipeRefreshLayout;
import id.telkomuniv.tel_usiagacovid_19.view.OurTeamActivity;
import id.telkomuniv.tel_usiagacovid_19.view.dashboard.DashboardActivity;
import id.telkomuniv.tel_usiagacovid_19.view.data_global.DataGlobalActivity;
import id.telkomuniv.tel_usiagacovid_19.view.data_prov.DataProvActivity;
import id.telkomuniv.tel_usiagacovid_19.view.hotline.HotlineActivity;
import id.telkomuniv.tel_usiagacovid_19.view.hotline.HotlineAdapter;
import id.telkomuniv.tel_usiagacovid_19.view.news_more.NewsMoreActivity;
import io.intercom.android.sdk.Intercom;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private static String TAG = "HomeFragment";

    @BindView(R.id.msrl_home)
    MultiSwipeRefreshLayout multiSwipeRefreshLayout;
    @BindView(R.id.tv_positif_indo_home)
    TextView tvPositifIndo;
    @BindView(R.id.tv_healthy_indo_home)
    TextView tvHealthyIndo;
    @BindView(R.id.tv_death_indo_home)
    TextView tvDeathIndo;
    @BindView(R.id.tv_positif_jabar_home)
    TextView tvPositifJabar;
    @BindView(R.id.tv_healthy_jabar_home)
    TextView tvHealthyJabar;
    @BindView(R.id.tv_death_jabar_home)
    TextView tvDeathJabar;
    @BindView(R.id.anyChartViewHome)
    AnyChartView anyChartView;
    @BindView(R.id.pbHome)
    ProgressBar progressBar;
    @BindView(R.id.rv_news_home)
    RecyclerView rvNews;

    private DashboardActivity activity;
    private ApiService apiService;
    private ApiService apiServiceTelu;
    private List<Global> globalList;
    private List<Province> provinceList;
    private List<Timeline> timelineList;
    private List<News> newsList;

    public HomeFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public HomeFragment(DashboardActivity dashboardActivity) {
        this.activity = dashboardActivity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        ButterKnife.bind(this, view);
        apiService = ApiConfig.getApiService(getContext());
        apiServiceTelu = ApiConfig.getApiServiceTelu(getContext());
        initMultiSwipeRefresh();
        //initIntercome();
        getDataIndo();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        //Intercom.client().setLauncherVisibility(Intercom.Visibility.VISIBLE);
    }

    @Override
    public void onPause() {
        super.onPause();
        //Intercom.client().setLauncherVisibility(Intercom.Visibility.GONE);
    }

    private void initMultiSwipeRefresh() {
        multiSwipeRefreshLayout.setSwipeableChildren(R.id.sv_home);
        multiSwipeRefreshLayout.setRefreshing(true);
        multiSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDataIndo();
            }
        });
    }

    private void initIntercome() {
        Intercom.initialize(activity.getApplication(), "android_sdk-3574b2712b43a2e89634101403394c7006587fa7", "ihhe2dqx");
        Intercom.client().registerUnidentifiedUser();
        Intercom.client().setLauncherVisibility(Intercom.Visibility.VISIBLE);
        Intercom.client().setBottomPadding(240);
    }

    private void getDataIndo() {
        globalList = new ArrayList<>();
        Call<List<Global>> call = apiService.indexGlobal();
        call.enqueue(new Callback<List<Global>>() {
            @Override
            public void onResponse(Call<List<Global>> call, Response<List<Global>> response) {
                if (response.isSuccessful()){
                    Log.e(TAG, "response success : " + response.raw());
                    Log.e(TAG, "response success : " + response.body());
                    for (int i=0; i<response.body().size(); i++){
                        globalList.add(response.body().get(i));
                    }

                    //data indonesia
                    tvPositifIndo.setText(globalList.get(0).getPositif());
                    tvHealthyIndo.setText(globalList.get(0).getSembuh());
                    tvDeathIndo.setText(globalList.get(0).getMeninggal());
                }
                else{
                    Log.e(TAG, "response is not success : " + response.raw());
                    Log.e(TAG, "response is not success : " + response.message());
                    Log.e(TAG, "response is not success : " + response.toString());
                    Log.e(TAG, "response is not success : " + response.code());
                    Log.e(TAG, "response is not success : " + response.errorBody());
                    Log.e(TAG, "response is not success : " + response.headers());
                    showToast(getResources().getString(R.string.failed_to_get_data));
                }
                getDataProvince();
            }

            @Override
            public void onFailure(Call<List<Global>> call, Throwable t) {
                Log.e(TAG, "onFailure : " + t.getMessage());
                showToast(getResources().getString(R.string.there_is_an_error));
                getDataProvince();
            }
        });
    }

    private void getDataProvince() {
        provinceList = new ArrayList<>();
        Call<List<ProvinceResponse>> call = apiService.indexProvince();
        call.enqueue(new Callback<List<ProvinceResponse>>() {
            @Override
            public void onResponse(Call<List<ProvinceResponse>> call, Response<List<ProvinceResponse>> response) {
                if (response.isSuccessful()){
                    Log.e(TAG, "response success : " + response.raw());
                    Log.e(TAG, "response success : " + response.body());
                    for (int i=0; i<response.body().size(); i++){
                        provinceList.add(response.body().get(i).getProvince());
                    }

                    //data jabar
                    for (int i = 0; i< provinceList.size(); i++){
                        if (provinceList.get(i).getKode_Provi()==32){
                            tvDeathJabar.setText(String.valueOf(provinceList.get(i).getKasus_Meni()));
                            tvHealthyJabar.setText(String.valueOf(provinceList.get(i).getKasus_Semb()));
                            tvPositifJabar.setText(String.valueOf(provinceList.get(i).getKasus_Posi()));
                        }
                    }
                }
                else{
                    Log.e(TAG, "response is not success : " + response.raw());
                    Log.e(TAG, "response is not success : " + response.message());
                    Log.e(TAG, "response is not success : " + response.toString());
                    Log.e(TAG, "response is not success : " + response.code());
                    Log.e(TAG, "response is not success : " + response.errorBody());
                    Log.e(TAG, "response is not success : " + response.headers());
                    showToast(getResources().getString(R.string.failed_to_get_data));
                    multiSwipeRefreshLayout.setRefreshing(false);
                }
                getDataTimeline();
            }

            @Override
            public void onFailure(Call<List<ProvinceResponse>> call, Throwable t) {
                Log.e(TAG, "onFailure : " + t.getMessage());
                showToast(getResources().getString(R.string.there_is_an_error));
                multiSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void getDataTimeline() {
        timelineList = new ArrayList<>();
        Call<DataResponse<TimelineResponse>> call = apiService.indexTimeline();
        call.enqueue(new Callback<DataResponse<TimelineResponse>>() {
            @Override
            public void onResponse(Call<DataResponse<TimelineResponse>> call, Response<DataResponse<TimelineResponse>> response) {
                if (response.isSuccessful()){
                    Log.e(TAG, "response success : " + response.raw());
                    Log.e(TAG, "response success : " + response.body());
                    timelineList = response.body().getData().getTimelines();

                    getDataNews();
                }
                else{
                    Log.e(TAG, "response is not success : " + response.raw());
                    Log.e(TAG, "response is not success : " + response.message());
                    Log.e(TAG, "response is not success : " + response.toString());
                    Log.e(TAG, "response is not success : " + response.code());
                    Log.e(TAG, "response is not success : " + response.errorBody());
                    Log.e(TAG, "response is not success : " + response.headers());
                    showToast(getResources().getString(R.string.failed_to_get_data));
                    multiSwipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<DataResponse<TimelineResponse>> call, Throwable t) {
                Log.e(TAG, "onFailure : " + t.getMessage());
                showToast(getResources().getString(R.string.there_is_an_error));
                multiSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void getDataNews() {
        newsList = new ArrayList<>();
        Call<List<News>> listCall = apiServiceTelu.indexNews();
        listCall.enqueue(new Callback<List<News>>() {
            @Override
            public void onResponse(Call<List<News>> call, Response<List<News>> response) {
                if (response.isSuccessful()){
                    Log.e(TAG, "response success : " + response.raw());
                    Log.e(TAG, "response success : " + response.body());
                    newsList = response.body();
                }
                else{
                    Log.e(TAG, "response is not success : " + response.raw());
                    Log.e(TAG, "response is not success : " + response.message());
                    Log.e(TAG, "response is not success : " + response.toString());
                    Log.e(TAG, "response is not success : " + response.code());
                    Log.e(TAG, "response is not success : " + response.errorBody());
                    Log.e(TAG, "response is not success : " + response.headers());
                    showToast(getResources().getString(R.string.failed_to_get_data));
                    multiSwipeRefreshLayout.setRefreshing(false);
                }
                showData();
            }

            @Override
            public void onFailure(Call<List<News>> call, Throwable t) {
                Log.e(TAG, "onFailure : " + t.getMessage());
                showToast(getResources().getString(R.string.there_is_an_error));
                multiSwipeRefreshLayout.setRefreshing(false);
                showData();
            }
        });
    }

    private void showData() {
        //data indonesia
        tvPositifIndo.setText(globalList.get(0).getPositif());
        tvHealthyIndo.setText(globalList.get(0).getSembuh());
        tvDeathIndo.setText(globalList.get(0).getMeninggal());

        //data jabar
        for (int i = 0; i< provinceList.size(); i++){
            if (provinceList.get(i).getKode_Provi()==32){
                tvDeathJabar.setText(String.valueOf(provinceList.get(i).getKasus_Meni()));
                tvHealthyJabar.setText(String.valueOf(provinceList.get(i).getKasus_Semb()));
                tvPositifJabar.setText(String.valueOf(provinceList.get(i).getKasus_Posi()));
            }
        }

        //data timeline
        List<Timeline> tmpList = new ArrayList<>();
        for (int i=timelineList.size()-1; i>=0; i--){
            tmpList.add(timelineList.get(i));
        }
        timelineList = tmpList;
        timelineList.remove(timelineList.size()-1);
        showDataTimeline();

        //data news
        /*newsList.add(newsList.get(0));
        newsList.add(newsList.get(0));
        newsList.add(newsList.get(0));
        newsList.add(newsList.get(0));
        newsList.add(newsList.get(0));
        newsList.add(newsList.get(0));
        newsList.add(newsList.get(0));
        newsList.add(newsList.get(0));*/
        List<News> newsListTemp = new ArrayList<>();
        if (newsList.size()>4){
            for (int i=0; i<4; i++){
                newsListTemp.add(newsList.get(i));
            }
        }
        else
            newsListTemp = newsList;

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        rvNews.setHasFixedSize(true);
        rvNews.setLayoutManager(linearLayoutManager);
        NewsHomeAdapter adapter = new NewsHomeAdapter(getContext(), newsListTemp);
        rvNews.setAdapter(adapter);

        multiSwipeRefreshLayout.setRefreshing(false);
    }

    private void showDataTimeline() {
        anyChartView.setProgressBar(progressBar);

        Cartesian cartesian = AnyChart.line();

        cartesian.animation(true);

        cartesian.padding(5d, 10d, 2d, 10d);

        cartesian.crosshair().enabled(true);
        cartesian.crosshair()
                .yLabel(true)
                // TODO ystroke
                .yStroke((Stroke) null, null, null, (String) null, (String) null);

        cartesian.tooltip().positionMode(TooltipPositionMode.POINT);

        cartesian.title("Statistik kasus positif COVID-19 di Indonesia");

        cartesian.yAxis(0).title("");
        cartesian.xAxis(0).labels().padding(5d, 5d, 25d, 5d);

        List<DataEntry> seriesData = new ArrayList<>();
        for (int i=0; i<timelineList.size(); i++) {
            seriesData.add(new CustomDataEntry(CommonUtils.customDateShort(getContext(), timelineList.get(i).getDate()), timelineList.get(i).getNew_confirmed(), timelineList.get(i).getConfirmed()));
        }

        Set set = Set.instantiate();
        set.data(seriesData);
        Mapping series1Mapping = set.mapAs("{ x: 'x', value: 'value' }");
        Mapping series2Mapping = set.mapAs("{ x: 'x', value: 'value2' }");

        Line series1 = cartesian.line(series1Mapping);
        series1.name("Kasus positif perhari");
        series1.hovered().markers().enabled(true);
        series1.hovered().markers()
                .type(MarkerType.CIRCLE)
                .size(4d);
        series1.tooltip()
                .position("right")
                .anchor(Anchor.LEFT_CENTER)
                .offsetX(5d)
                .offsetY(5d);

        Line series2 = cartesian.line(series2Mapping);
        series2.name("Total positif");
        series2.hovered().markers().enabled(true);
        series2.hovered().markers()
                .type(MarkerType.CIRCLE)
                .size(4d);
        series2.tooltip()
                .position("right")
                .anchor(Anchor.LEFT_CENTER)
                .offsetX(5d)
                .offsetY(5d);

        cartesian.legend().enabled(true);
        cartesian.legend().fontSize(13d);
        cartesian.legend().padding(0d, 0d, 10d, 0d);

        anyChartView.setChart(cartesian);

        multiSwipeRefreshLayout.setRefreshing(false);
    }

    private class CustomDataEntry extends ValueDataEntry {

        CustomDataEntry(String x, Number value, Number value2) {
            super(x, value);
            setValue("value2", value2);
        }

    }

    private void showToast(String s){
        Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.cv_search_prov_home) void cvSearchProvOnClick(){
        startActivity(new Intent(getContext(), DataProvActivity.class));
    }

    @OnClick(R.id.cv_global_home) void cvGlobalOnClick(){
        startActivity(new Intent(getContext(), DataGlobalActivity.class));
    }

    @OnClick(R.id.cv_hotline_home) void cvHotlineOnClick(){
        startActivity(new Intent(getContext(), HotlineActivity.class));
    }

    @OnClick(R.id.cv_team_home) void cvTeamOnClick(){
        startActivity(new Intent(getContext(), OurTeamActivity.class));
    }

    @OnClick(R.id.cv_news_more_home) void cvNewsMoreOnClick(){
        if (newsList!=null){
            if (!newsList.isEmpty()){
                Intent intent = new Intent(getContext(), NewsMoreActivity.class);
                intent.putExtra(Constant.PUT_EXTRA_NEWS_LIST, (Serializable) newsList);
                startActivity(intent);
            }
        }
    }

}
