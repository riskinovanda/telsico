package id.telkomuniv.tel_usiagacovid_19.view.data_prov;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.telkomuniv.tel_usiagacovid_19.R;
import id.telkomuniv.tel_usiagacovid_19.model.Country;
import id.telkomuniv.tel_usiagacovid_19.model.CountryResponse;
import id.telkomuniv.tel_usiagacovid_19.model.Province;
import id.telkomuniv.tel_usiagacovid_19.model.ProvinceResponse;
import id.telkomuniv.tel_usiagacovid_19.rest.ApiConfig;
import id.telkomuniv.tel_usiagacovid_19.rest.ApiService;
import id.telkomuniv.tel_usiagacovid_19.utils.MultiSwipeRefreshLayout;
import id.telkomuniv.tel_usiagacovid_19.view.data_global.DataGlobalAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataProvActivity extends AppCompatActivity {

    private static String TAG = "DataProvActivity";

    @BindView(R.id.msrl_data_prov)
    MultiSwipeRefreshLayout multiSwipeRefreshLayout;
    @BindView(R.id.rv_data_prov)
    RecyclerView recyclerView;

    private ApiService apiService;
    private List<Province> provinceList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_prov);

        ButterKnife.bind(this);
        apiService = ApiConfig.getApiService(this);
        initMultiSwipeRefresh();
        getData();
    }

    private void initMultiSwipeRefresh() {
        multiSwipeRefreshLayout.setSwipeableChildren(R.id.rv_data_prov);
        multiSwipeRefreshLayout.setRefreshing(true);
        multiSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });
    }

    private void getData() {
        provinceList = new ArrayList<>();
        Call<List<ProvinceResponse>> call = apiService.indexProvince();
        call.enqueue(new Callback<List<ProvinceResponse>>() {
            @Override
            public void onResponse(Call<List<ProvinceResponse>> call, Response<List<ProvinceResponse>> response) {
                if (response.isSuccessful()){
                    Log.e(TAG, "response success : " + response.raw());
                    Log.e(TAG, "response success : " + response.body());
                    for (int i=0; i<response.body().size(); i++){
                        provinceList.add(response.body().get(i).getProvince());
                    }
                    showData();
                }
                else{
                    Log.e(TAG, "response is not success : " + response.raw());
                    Log.e(TAG, "response is not success : " + response.message());
                    Log.e(TAG, "response is not success : " + response.toString());
                    Log.e(TAG, "response is not success : " + response.code());
                    Log.e(TAG, "response is not success : " + response.errorBody());
                    Log.e(TAG, "response is not success : " + response.headers());
                    showToast(getResources().getString(R.string.failed_to_get_data));
                    multiSwipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<List<ProvinceResponse>> call, Throwable t) {
                Log.e(TAG, "onFailure : " + t.getMessage());
                showToast(getResources().getString(R.string.there_is_an_error));
                multiSwipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    private void showData() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        DataProvAdapter adapter = new DataProvAdapter(this, provinceList);
        recyclerView.setAdapter(adapter);

        multiSwipeRefreshLayout.setRefreshing(false);
    }

    @OnClick(R.id.cv_back_data_prov) void cvBackOnClick(){
        onBackPressed();
    }

    private void showToast(String s){
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }
}
