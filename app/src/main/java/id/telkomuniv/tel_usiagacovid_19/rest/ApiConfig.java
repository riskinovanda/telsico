package id.telkomuniv.tel_usiagacovid_19.rest;

import android.content.Context;

import com.pixplicity.easyprefs.library.Prefs;

public class ApiConfig {

    public static final String API_URL = "https://covid.rasyid.in";
    public static final String API_URL_TELU = "https://telu.rasyid.in";

    public static ApiService getApiService(Context context){
        return ApiClient.getClient(API_URL).create(ApiService.class);
    }

    public static ApiService getApiServiceTelu(Context context){
        return ApiClient.getClient(API_URL_TELU).create(ApiService.class);
    }

    /*public static String getAuth(Context context) {
        new Prefs.Builder()
                .setContext(context)
                .setMode(android.content.ContextWrapper.MODE_PRIVATE)
                .setPrefsName(context.getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();

        return Prefs.getString(Constant.PREF_TOKEN, "");
    }*/

}
