package id.telkomuniv.tel_usiagacovid_19.rest;

import java.util.List;

import id.telkomuniv.tel_usiagacovid_19.model.AppVersion;
import id.telkomuniv.tel_usiagacovid_19.model.CountryResponse;
import id.telkomuniv.tel_usiagacovid_19.model.DataResponse;
import id.telkomuniv.tel_usiagacovid_19.model.Faq;
import id.telkomuniv.tel_usiagacovid_19.model.Global;
import id.telkomuniv.tel_usiagacovid_19.model.Hotline;
import id.telkomuniv.tel_usiagacovid_19.model.HotlinePrimary;
import id.telkomuniv.tel_usiagacovid_19.model.News;
import id.telkomuniv.tel_usiagacovid_19.model.Policy;
import id.telkomuniv.tel_usiagacovid_19.model.ProvinceResponse;
import id.telkomuniv.tel_usiagacovid_19.model.TimelineResponse;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {

    //data province
    @GET("api/indonesia/provinsi")
    Call<List<ProvinceResponse>> indexProvince();

    //data global
    @GET("api/indonesia")
    Call<List<Global>> indexGlobal();

    //data national timeline
    @GET("api/hariini")
    Call<DataResponse<TimelineResponse>> indexTimeline();

    //data policy
    @GET("api/allKebijakan")
    Call<List<Policy>> indexPolicy();

    //data country
    @GET("api/global")
    Call<List<CountryResponse>> indexCountry();

    //data app version
    @GET("api/version")
    Call<List<AppVersion>> indexAppVersion();

    //data callcenter
    @GET("api/callcentre")
    Call<List<Hotline>> indexHotline();

    //data Faq
    @GET("api/faq")
    Call<List<Faq>> indexFaq();

    //data hotline primary
    @GET("api/hotline")
    Call<List<HotlinePrimary>> indexHotlinePrimary();

    //data berita
    @GET("api/berita")
    Call<List<News>> indexNews();

   /* //register calls    ----------------------------------------------------------------------------
    @FormUrlEncoded
    @POST("auth/register")
    Call<RegisterResponse<User>> postRegister(@Field("username") String username, @Field("phone") String phone, @Field("email") String email,
                                              @Field("password") String password, @Field("credentials") String credentials);

    //login calls       ----------------------------------------------------------------------------
    @FormUrlEncoded
    @POST("auth/login")
    Call<LoginResponse<User>> postLogin(@Field("username") String username, @Field("password") String password);


    //branch calls      ----------------------------------------------------------------------------
    @GET("customer/branch")
    Call<List<Branch>> indexBranch(@Header("Authorization") String auth);

    @GET("customer/branch?")
    Call<Branch> showBranch(@Header("Authorization") String auth, @Query("branchId") String branchId);



    //trademark calls   ----------------------------------------------------------------------------
    @GET("customer/trademark")
    Call<List<Trademark>> indexTrademark(@Header("Authorization") String auth);

    @GET("customer/trademark?")
    Call<Trademark> showTrademark(@Header("Authorization") String auth, @Query("trademarkId") String trademarkId);

    //transaction table calls ----------------------------------------------------------------------
    @GET("customer/transaction/table?")
    Call<Trademark> showTransactionTable(@Header("Authorization") String auth, @Query("tableId") String tableId);

    //transaction calls -----------------------------------------------------------------------

    @POST("customer/transaction/create")
    Call<TransactionResponse> storeTransaction(@Header("Authorization") String auth, @Body Transaction transaction);

    @POST("customer/transaction/bri/create")
    Call<BrivaResponse> storeTransactionBriva(@Header("Authorization") String auth, @Body TransactionBrivaStore transactionBrivaStore);

    @GET("customer/transaction/history?status=1&status=2&status=3&status=4")
    Call<List<Transaction>> indexTransactionProcess(@Header("Authorization") String auth);

    @GET("customer/transaction/history?status=5&status=6")
    Call<List<Transaction>> indexTransactionHistory(@Header("Authorization") String auth);

    @GET("customer/transaction/history?status=1&status=2&status=3&status=4&length=3")
    Call<List<Transaction>> indexPageTransactionProcess(@Header("Authorization") String auth);

    @GET("customer/transaction/history?status=5&status=6&length=5")
    Call<List<Transaction>> indexPageTransactionHistory(@Header("Authorization") String auth);*/
}
